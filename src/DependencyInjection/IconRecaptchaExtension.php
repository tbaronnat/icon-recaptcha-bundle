<?php

namespace TBaronnat\IconRecaptchaBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;


class IconRecaptchaExtension extends ConfigurableExtension
{
    public function loadInternal(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();

        foreach ($configs as $key => $value) {
            $container->setParameter('tbaronnat_icon_recaptcha.' . $key, $value);
        }
        $resources = $container->getParameter('twig.form.resources');

        $container->setParameter(
            'twig.form.resources',
            array_merge(['@TBaronnatIconRecaptcha/form/icon_recaptcha.html.twig'], $resources)
        );
    }

    public function getAlias(): string
    {
        return 'tbaronnat_icon_recaptcha_bundle';
    }
}