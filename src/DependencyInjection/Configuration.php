<?php

namespace TBaronnat\IconRecaptchaBundle\DependencyInjection;


use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use TBaronnat\IconRecaptchaBundle\Manager\CaptchaOptions;

class Configuration implements ConfigurationInterface
{
    function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('tbaronnat_icon_recaptcha_bundle');

        $treeBuilder
            ->getRootNode()
            ->children()
                ->arrayNode('options')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('secret')->cannotBeEmpty()->isRequired()->end() //Secret use to generate random token
                        ->scalarNode('iconPath')->cannotBeEmpty()->isRequired()->end() //icons are on ../../assets/icons
                        ->scalarNode('transdomain') //Messages translation domain
                            ->cannotBeEmpty()
                            ->defaultValue("tbaronnat_icon_recaptcha")
                        ->end()
                        ->arrayNode('messages')->addDefaultsIfNotSet() //Error messages
                            ->children()
                                ->scalarNode('wrong_icon')
                                    ->cannotBeEmpty()
                                    ->defaultValue("icon_recaptcha.verification.error.wrong_icon")
                                ->end()
                                ->scalarNode('no_selection')
                                    ->cannotBeEmpty()
                                    ->defaultValue("icon_recaptcha.verification.error.no_selection")
                                ->end()
                                ->scalarNode('empty_form')
                                    ->cannotBeEmpty()
                                    ->defaultValue("icon_recaptcha.verification.error.empty_form")
                                ->end()
                                ->scalarNode('invalid_id')
                                    ->cannotBeEmpty()
                                    ->defaultValue("icon_recaptcha.verification.error.invalid_id")
                                ->end()
                                ->scalarNode('form_token')
                                    ->cannotBeEmpty()
                                    ->defaultValue("icon_recaptcha.verification.error.form_token")
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('image')->addDefaultsIfNotSet() //Images recaptcha configuration
                            ->children()
                                ->scalarNode('size')
                                    ->cannotBeEmpty()
                                    ->defaultValue(CaptchaOptions::CAPTCHA_DEFAULT_IMAGE_SIZE)
                                ->end()
                                ->arrayNode('amount')->addDefaultsIfNotSet() //Number of images displayed on recaptcha
                                    ->children()
                                        ->scalarNode('min')//>=5, <=8
                                            ->cannotBeEmpty()
                                            ->defaultValue(CaptchaOptions::CAPTCHA_MIN_ICON_COUNT)
                                        ->end()
                                        ->scalarNode('max')//>=5, <=8, >= min
                                            ->cannotBeEmpty()
                                            ->defaultValue(CaptchaOptions::CAPTCHA_MAX_ICON_COUNT)
                                        ->end()
                                    ->end()
                                ->end()
                                ->arrayNode('flip')->addDefaultsIfNotSet() //Images transformations
                                    ->children()
                                        ->scalarNode('horizontally')->cannotBeEmpty()->defaultTrue()->end()
                                        ->scalarNode('vertically')->cannotBeEmpty()->defaultTrue()->end()
                                    ->end()
                                ->end()
                                ->scalarNode('rotate')->cannotBeEmpty()->defaultFalse()->end()
                                ->scalarNode('border')->cannotBeEmpty()->defaultTrue()->end()
                            ->end()
                        ->end()
                        ->arrayNode('attempts')->addDefaultsIfNotSet() //Security
                            ->children()
                                ->scalarNode('timeout') //Time to wait if to many attemps, in seconds
                                    ->cannotBeEmpty()
                                    ->defaultValue(CaptchaOptions::CAPTCHA_DEFAULT_TIMEOUT)
                                ->end()
                                ->scalarNode('amount') //Nb attemps before timeout
                                    ->cannotBeEmpty()
                                    ->defaultValue(CaptchaOptions::CAPTCHA_DEFAULT_MAX_ATTEMPTS)
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}