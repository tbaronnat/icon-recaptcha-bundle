<?php

namespace TBaronnat\IconRecaptchaBundle\Validator\Constraints;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use TBaronnat\IconRecaptchaBundle\Manager\IconReCaptcha;

class IsValidCaptchaValidator extends ConstraintValidator
{
    public function __construct(
        private readonly IconReCaptcha $reCaptcha,
        private readonly RequestStack $requestStack,
        private readonly TranslatorInterface $translator
    ) {}

    public function validate($value, Constraint $constraint)
    {
        $request = $this->requestStack->getMainRequest();
        $result = $this->reCaptcha::validateSubmission($request);

        if (!$result) {
            $transDomain = $this->reCaptcha::getCaptchaOptions()::getTranslationDomain();
            $this->context->addViolation(
                $this->translator->trans(
                    $this->reCaptcha::getCaptchaError()::getMessage(), [], $transDomain, $request->getLocale()
                )
            );
        }
    }
}
