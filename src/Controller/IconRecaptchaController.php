<?php

namespace TBaronnat\IconRecaptchaBundle\Controller;

use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use TBaronnat\IconRecaptchaBundle\Manager\CaptchaToken;
use TBaronnat\IconRecaptchaBundle\Manager\IconReCaptcha;

class IconRecaptchaController extends AbstractController
{
    const HTTP_STATUS_OK = 'HTTP/1.0 200 OK';

    #[NoReturn] public function listen(Request $request, IconReCaptcha $iconReCaptcha): void
    {
        if (CaptchaToken::getPayload($request) !== null) {
            // HTTP GET
            if ($request->isMethod(Request::METHOD_GET) && !$request->isXmlHttpRequest()) {
                $this->listenGetRequest($request, $iconReCaptcha);
            }

            // HTTP POST
            if ($request->isMethod(Request::METHOD_POST) && $request->isXmlHttpRequest()) {
                $this->listenPostRequest($request, $iconReCaptcha);
            }
        }

        $this->throwBadRequest();
    }

    #[NoReturn] private function listenGetRequest(Request $request, IconReCaptcha $iconReCaptcha): void
    {
        // Decode the payload.
        $payload = $iconReCaptcha::$token::decodePayload($request);

        // Validate the payload content.
        if (!$payload || !isset($payload, $payload['i']) || !is_numeric($payload['i'])) {
            $this->throwBadRequest();
        }

        // Validate the captcha token.
        if (!$iconReCaptcha::$token::validateToken($request, $payload['tk'] ?? [])) {
            $this->throwTokenError();
        }

        $iconReCaptcha::getImage($request, $payload['i']);
        exit;
    }

    private function listenPostRequest(Request $request, IconReCaptcha $iconReCaptcha): void
    {
        // Decode the payload.
        $payload = $iconReCaptcha::$token::decodePayload($request);

        // Validate the payload content.
        if (!$payload || !isset($payload, $payload['a'], $payload['i']) || !is_numeric($payload['a']) || !is_numeric($payload['i'])) {
            $this->throwBadRequest();
        }

        // Validate the captcha token.
        if (!$iconReCaptcha::$token::validateToken($request, $payload['tk'] ?? [])) {
            $this->throwTokenError();
        }

        switch ((int)$payload['a']) {
            case 1: // Requesting the image hashes

                // Validate the theme name. Fallback to light.
                $theme = (isset($payload['t']) && is_string($payload['t'])) ? $payload['t'] : 'light';
                // Echo the captcha data.
                header(self::HTTP_STATUS_OK);
                header('Content-type: text/plain');
                exit($iconReCaptcha::getCaptchaData($request, $theme, $payload['i']));
            case 2: // Setting the user's choice
                if ($iconReCaptcha::setSelectedAnswer($request, $payload)) {
                    header(self::HTTP_STATUS_OK);
                    exit;
                }
                break;
            case 3: // Captcha interaction time expired.
                IconReCaptcha::invalidateSession($request, $payload['i']);
                header(self::HTTP_STATUS_OK);
                exit;
            default:
                break;
        }
    }

    // Create a error message string for the token validation error.
    #[NoReturn] private function throwTokenError(): void
    {
        header(self::HTTP_STATUS_OK);
        header('Content-type: text/plain');
        exit(base64_encode(json_encode(['error' => 2])));
    }


    // Exits the request with a 400 bad request status.
    #[NoReturn] private function throwBadRequest(): void
    {
        header('HTTP/1.1 400 Bad Request');
        exit;
    }
}