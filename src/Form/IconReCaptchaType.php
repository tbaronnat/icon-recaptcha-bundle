<?php

namespace TBaronnat\IconRecaptchaBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TBaronnat\IconRecaptchaBundle\Manager\CaptchaOptions;
use TBaronnat\IconRecaptchaBundle\Manager\IconReCaptcha;
use TBaronnat\IconRecaptchaBundle\Validator\Constraints\IsValidCaptcha;

class IconReCaptchaType extends AbstractType
{
    public function __construct(private readonly IconReCaptcha $iconReCaptcha)
    {}

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['token'] = $options['token'];
        $view->vars['options'] = $this->iconReCaptcha::$options;
        $view->vars['theme'] = $options['theme'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('token', true)
            ->setDefault('theme', CaptchaOptions::THEME_LIGHT)
            ->setDefault('mapped', false)
            ->setDefault('error_bubbling', false)
            ->setDefault('constraints', new IsValidCaptcha());
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?string
    {
        return HiddenType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'icon_recaptcha';
    }
}
