<?php

namespace TBaronnat\IconRecaptchaBundle;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TBaronnat\IconRecaptchaBundle\DependencyInjection\IconRecaptchaExtension;

class TBaronnatIconRecaptchaBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        $this->extension = new IconRecaptchaExtension();

        return $this->extension;
    }
}