<?php

namespace TBaronnat\IconRecaptchaBundle\Manager;

use Symfony\Component\HttpFoundation\Request;

class CaptchaToken
{
    public const SESSION_TOKEN = 'token';
    private static ?string $secret;

    /**
     * secret is defined in options, used to generate the token hash
     */
    public function __construct(?string $secret = null)
    {
        self::$secret = $secret ?? uniqid(time(), true);
    }

    /**
     * Generates and returns a secure random string which will serve as a CSRF token for the current session. After
     * generating the token, it will be saved in the global session variable. A token will only be generated
     * when no token has been generated before in the current session. If a token already exists, this token will
     * be returned instead.
     *
     * @return ?string The captcha token.
     */
    public static function buildToken(Request $request): ?string
    {
        $token = self::getToken($request);

        // Make sure to only generate a token if none exists.
        if ($token == null) {
            self::setToken($request, self::generateTokenHash());
        }

        return self::getToken($request);
    }


    /**
     * Returns the captcha session/CSRF token.
     * @return string|null A token as a string, or NULL if no token exists.
     */
    public static function getToken(Request $request): ?string
    {
        return $request->getSession()->get(self::SESSION_TOKEN);
    }

    /**
     * Save token on session
     */
    public static function setToken(Request $request, string $token): void
    {
        $request->getSession()->set(self::SESSION_TOKEN, $token);
    }

    /**
     * Returns captcha payload from request
     * @param Request $request
     * @return string|null
     */
    public static function getPayload(Request $request): string|null
    {
        return $request->get('payload');
    }

    /**
     * ries to decode the given base64 and json encoded payload.
     * @param Request $request
     * @return string|bool
     */
    public static function decodePayload(Request $request): mixed
    {
        // Base64 decode the payload.
        $payload = base64_decode(self::getPayload($request));
        if ($payload === false) {
            return false;
        }

        // JSON decode the payload.
        return json_decode($payload, true);
    }

    /**
     * Validates the global captcha session token against the given payload token and sometimes against a header token
     * as well. All the given tokens must match the global captcha session token to pass the check. This function
     * will only validate the given tokens if the 'token' option is set to TRUE. If the 'token' option is set to anything
     * else other than TRUE, the check will be skipped.
     *
     * @param ?string $payloadToken The token string received via the HTTP request body.
     * @return bool TRUE if the captcha session token matches the given tokens or if the token option is disabled,
     * FALSE if the captcha session token does not match the given tokens.
     */
    public static function validateToken(Request $request, ?string $payloadToken): bool
    {
        // Only validate if the token option is enabled.
        if (!empty($payloadToken)) {
            $sessionToken = self::getToken($request);

            // If the token is empty but the option is enabled, the token was never requested.
            if (empty($sessionToken)) {
                return false;
            }

            // Validate the payload against the session token.
            return $sessionToken === $payloadToken;
        }

        return true;
    }

    /**
     * Generate new token
     */
    private static function generateTokenHash(): string
    {
        return str_shuffle(
            md5(
                bin2hex(
                    base64_encode(
                        hash_hmac('sha256', uniqid(), self::$secret, true)
                    )
                )
            )
        );
    }
}
