<?php

namespace TBaronnat\IconRecaptchaBundle\Manager;

class CaptchaError
{
    private static string $error;

    /**
     * Returns the validation error message, or return NULL if there is no error.
     *
     * @return string|null The JSON encoded error message containing the error ID and message, or NULL.
     */
    public static function getMessage(): ?string
    {
        return !empty(self::$error) ? json_decode(self::$error)->error : null;
    }

    /**
     * Sets the global {@see $error} property, which can be retrieved with the {@see getMessage} function.
     * @param int $id The identifier of the error message.
     * @param string $message The error message to set.
     */
    public static function setMessage(int $id, string $message): void
    {
        self::$error = json_encode(['id' => $id, 'error' => $message]);
    }
}
