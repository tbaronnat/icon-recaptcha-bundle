<?php

namespace TBaronnat\IconRecaptchaBundle\Manager;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class IconReCaptcha
{
    public static CaptchaOptions $options;
    public static CaptchaError $error;
    public static CaptchaToken $token;
    private static CaptchaSession $session;

    public function __construct(array $options)
    {
        self::$options = new CaptchaOptions($options);
        self::$token = new CaptchaToken(self::$options->getSecret());
        self::$error = new CaptchaError();
    }

    /**
     * @return CaptchaOptions
     */
    public static function getCaptchaOptions(): CaptchaOptions
    {
        return self::$options;
    }

    /**
     * @return CaptchaError
     */
    public static function getCaptchaError(): CaptchaError
    {
        return self::$error;
    }
    /**
     * @return CaptchaToken
     */
    public static function getCaptchaToken(): CaptchaToken
    {
        return self::$token;
    }

    /**
     * Initializes the state of a captcha. The amount of icons shown in the captcha image, their positions,
     * which icon is correct and which icon identifiers should be used will all be determined in this function.
     * This information will be stored in the {@see CaptchaSession}. The details required to initialize the client
     * will be returned as a base64 encoded JSON string.
     *
     * In case a timeout is detected, no state will be initialized and an error message
     * will be returned, also as a base64 encoded JSON string.
     *
     * @param string $theme The theme of the captcha.
     * @param ?int $identifier The identifier of the captcha.
     *
     * @return string Captcha details required to initialize the client.
     */
    public static function getCaptchaData(Request $request, string $theme, ?int $identifier = 0): string
    {
        // Set the captcha id property
        self::createSession($request, $identifier);

        // Check if the max attempts limit has been reached and a timeout is active.
        // If reached, return an error and the remaining time.
        if (self::$session->attemptsTimeout > 0) {
            if (time() <= self::$session->attemptsTimeout) {
                return base64_encode(json_encode([
                    'error' => 1, 'data' => (self::$session->attemptsTimeout - time()) * 1000 // remaining time.
                ]));
            } else {
                self::$session->attemptsTimeout = 0;
                self::$session->attempts = 0;
            }
        }

        $minIconAmount = self::$options::getImageAmountMin();
        $maxIconAmount = self::$options::getImageAmountMax();

        // Determine the number of icons to add to the image.
        $iconAmount = $minIconAmount;
        if ($minIconAmount !== $maxIconAmount) {
            $iconAmount = mt_rand($minIconAmount, $maxIconAmount);
        }

        // Number of times the correct image will be placed onto the placeholder.
        $correctIconAmount = mt_rand(1, self::$options::CAPTCHA_MAX_LOWEST_ICON_COUNT[$iconAmount]);
        $totalIconAmount = self::calculateIconAmounts($iconAmount, $correctIconAmount);
        $totalIconAmount[] = $correctIconAmount;

        // Icon position and ID information.
        $iconPositions = [];
        $iconIds = [];
        $correctIconId = -1;

        // Create a random 'icon position' order.
        $tempPositions = range(1, $iconAmount);
        shuffle($tempPositions);

        // Generate the icon positions/IDs array.
        $i = 0;
        while (count($iconIds) < count($totalIconAmount)) {

            // Generate a random icon ID. If it is not in use yet, process it.
            $tempIconId = mt_rand(1, self::$options::CAPTCHA_ICONS_FOLDER_COUNT);
            if (!in_array($tempIconId, $iconIds)) {
                $iconIds[] = $tempIconId;

                // Assign the current icon ID to one or more positions.
                for ($j = 0; $j < $totalIconAmount[$i]; $j++) {
                    $tempKey = array_pop($tempPositions);
                    $iconPositions[$tempKey] = $tempIconId;
                }

                // Set the least appearing icon ID as the correct icon ID.
                if ($correctIconId === -1 && min($totalIconAmount) === $totalIconAmount[$i]) {
                    $correctIconId = $tempIconId;
                }

                $i++;
            }
        }

        // Get the last attempts count to restore, after clearing the session.
        $attemptsCount = self::$session->attempts;

        // Unset the previous session data.
        self::$session->clear();

        // Set the chosen icons and position and reset the requested status.
        self::$session->mode = $theme;
        self::$session->icons = $iconPositions;
        self::$session->iconIds = $iconIds;
        self::$session->correctId = $correctIconId;
        self::$session->requested = false;
        self::$session->attempts = $attemptsCount;
        self::$session->save($request);

        // Return the captcha details.
        return base64_encode(json_encode([
            'id' => $identifier
        ]));
    }

    /**
     * Validates the user form submission. If the captcha is incorrect, it
     * will set the global error variable and return FALSE, else TRUE.
     *
     * @param Request $request
     *
     * @return boolean TRUE if the captcha was correct, FALSE if not.
     */
    public static function validateSubmission(Request $request): bool
    {
        $post = $request->request->all();
        // Make sure the form data is set.
        if (empty($post)) {
            self::$error::setMessage(3, self::$options::getMessages()->empty_form);
            return false;
        }

        // Check if the captcha ID is set.
        if (!isset($post[self::$options::CAPTCHA_FIELD_ID]) || !is_numeric($post[self::$options::CAPTCHA_FIELD_ID])
            || !CaptchaSession::exists($request, CaptchaSession::SESSION_NAME, $post[self::$options::CAPTCHA_FIELD_ID])) {
            self::$error::setMessage(4, self::$options::getMessages()->invalid_id);
            return false;
        }

        // Check if the honeypot value is set.
        if (!isset($post[self::$options::CAPTCHA_FIELD_HONEYPOT]) || !empty($post[self::$options::CAPTCHA_FIELD_HONEYPOT])) {
            self::$error::setMessage(5, self::$options::getMessages()->invalid_id);
            return false;
        }

        // Verify if the captcha token is correct.
        $token = $post[self::$options::CAPTCHA_FIELD_TOKEN] ?? null;
        if (!self::$token::validateToken($request, $token)) {
            self::$error::setMessage(6, self::$options::getMessages()->form_token);
            return false;
        }

        // Get the captcha identifier.
        $identifier = $post[self::$options::CAPTCHA_FIELD_ID];

        // Initialize the session.
        self::createSession($request, $identifier);

        // Check if the selection field is set.
        if (!empty($post[self::$options::CAPTCHA_FIELD_SELECTION]) && is_string($post[self::$options::CAPTCHA_FIELD_SELECTION])) {

            // Parse the selection.
            $selection = explode(',', $post[self::$options::CAPTCHA_FIELD_SELECTION]);
            if (count($selection) === 3) {
                $clickedPosition = self::determineClickedIcon($selection[0], $selection[1], $selection[2], count(self::$session->icons));
            }

            // If the clicked position matches the stored position, the form can be submitted.
            if (self::$session->completed === true &&
                (isset($clickedPosition) && self::$session->icons[$clickedPosition] === self::$session->correctId)) {

                // Invalidate the captcha to prevent resubmission of a form on the same captcha.
                self::invalidateSession($request, $identifier);
                return true;
            } else {
                self::$error::setMessage(1, self::$options::getMessages()->wrong_icon);
            }
        } else {
            self::$error::setMessage(2, self::$options::getMessages()->no_selection);
        }

        return false;
    }

    /**
     * Checks if the by the user selected icon is the correct icon. Whether the clicked icon is correct or not
     * will be determined based on the clicked X and Y coordinates and the width of the IconReCaptcha DOM element.
     *
     * If the selected icon is indeed the correct icon, the {@see CaptchaSession} linked to the captcha identifier
     * will be marked as completed and TRUE will be returned. If an incorrect icon was selected, the session will
     * be marked as incomplete, the 'attempts' counter will be incremented by 1 and FALSE will be returned.
     *
     * A check will also take place to see if a timeout should set for the user, based on the options and attempts counter.
     *
     * @param array $payload The payload of the HTTP Post request, containing the captcha identifier, clicked X
     * and X coordinates and the width of the captcha element.
     *
     * @return boolean TRUE if the correct icon was selected, FALSE if not.
     */
    public static function setSelectedAnswer(Request $request, array $payload = []): bool
    {
        if (!empty($payload)) {

            // Check if the captcha ID and required other payload data is set.
            if (!isset($payload['i'], $payload['x'], $payload['y'], $payload['w'])) {
                return false;
            }

            // Initialize the session.
            self::createSession($request, $payload['i']);

            // Get the clicked position.
            $clickedPosition = self::determineClickedIcon($payload['x'], $payload['y'], $payload['w'], count(self::$session->icons));

            // Check if the selection is set and matches the position from the session.
            if (self::$session->icons[$clickedPosition] === self::$session->correctId) {
                self::$session->attempts = 0;
                self::$session->attemptsTimeout = 0;
                self::$session->completed = true;
                self::$session->save($request);

                return true;
            } else {
                self::$session->completed = false;
                // Increase the attempts counter.
                self::$session->attempts += 1;
                // If the max amount has been reached, set a timeout (if set).
                if (self::$session->attempts === self::$options::getAttemptsAmount()
                    && self::$options::getAttemptsTimeout() > 0) {
                    self::$session->attemptsTimeout = time() + self::$options::getAttemptsTimeout();
                }

                self::$session->save($request);
            }
        }

        return false;
    }

    /**
     * Invalidates the {@see CaptchaSession} linked to the given captcha identifier.
     * The data stored inside the session will be destroyed, as the session will be unset.
     *
     * @param ?int $identifier The identifier of the captcha.
     */
    public static function invalidateSession(Request $request, ?int $identifier = null): void
    {
        // Unset the previous session data
        self::createSession($request, $identifier);
        self::$session->destroy($request);
    }

    /**
     * Displays an image containing multiple icons in a random order for the current captcha instance, linked
     * to the given captcha identifier. Headers will be set to prevent caching of the image. In case the captcha
     * image was already requested once, a HTTP status '403 Forbidden' will be set and no image will be returned.
     *
     * The image will only be rendered once as a PNG, and be destroyed right after rendering.
     *
     * @param ?int $identifier The identifier of the captcha.
     */
    public static function getImage(Request $request, ?int $identifier = null): void
    {
        // Check if the captcha id is set
        if (isset($identifier) && $identifier > -1) {

            // Initialize the session.
            self::createSession($request, $identifier);

            // Check the amount of times an icon has been requested
            if (self::$session->requested) {
                throw new AccessDeniedException();
            }

            self::$session->requested = true;
            self::$session->save($request);

            $iconsDirectoryPath = self::$options::getIconPath();
            $placeholder = $iconsDirectoryPath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . self::$options::CAPTCHA_IMAGE_PLACEHOLDER_NAME;

            // Check if the placeholder icon exists.
            if (is_file($placeholder)) {

                // Format the path to the icon directory.
                $themeIconColor = self::$options::CAPTCHA_DEFAULT_THEME_COLORS[self::$session->mode]['icons'];
                $iconPath = $iconsDirectoryPath . DIRECTORY_SEPARATOR . $themeIconColor . DIRECTORY_SEPARATOR;

                // Generate the captcha image.
                $generatedImage = self::generateImage($iconPath, $placeholder);

                // Set the content type header to the PNG MIME-type.
                header('Content-type: image/png');

                // Disable caching of the image.
                header('Expires: 0');
                header('Cache-Control: no-cache, no-store, must-revalidate');
                header('Cache-Control: post-check=0, pre-check=0', false);
                header('Pragma: no-cache');

                // Show the image and exit the code
                imagepng($generatedImage);
                imagedestroy($generatedImage);
            }
        }
    }

    /**
     * Returns a generated image containing the icons for the current captcha instance. The icons will be copied
     * onto a placeholder image, located at the $placeholderPath. The icons will be randomly rotated and flipped
     * based on the captcha options.
     *
     * @param string $iconPath The path to the folder holding the icons.
     * @param string $placeholderPath The path to the placeholder image, with the name of the file included.
     * @return false|\GdImage|resource The generated image.
     */
    private static function generateImage(string $iconPath, string $placeholderPath)
    {
        // Prepare the placeholder image.
        $placeholder = imagecreatefrompng($placeholderPath);

        // Prepare the icon images.
        $iconImages = [];
        foreach (self::$session->iconIds as $id) {
            $iconImages[$id] = imagecreatefrompng(sprintf('%sicon-%s.png', $iconPath, $id));
        }

        // Image pixel information.
        $iconCount = count(self::$session->icons);
        $iconSize = self::$options::CAPTCHA_ICON_SIZES[$iconCount];
        $imageSize = self::$options::getImageSize();
        $iconOffset = (int)((($imageSize / $iconCount) - 30) / 2);
        $iconOffsetAdd = (int)(($imageSize / $iconCount) - $iconSize);
        $iconLineSize = (int)($imageSize / $iconCount);

        // Options.
        $borderEnabled = self::$options::useImageBorder();

        // Create the border color, if enabled.
        if ($borderEnabled) {
            $color = self::$options::CAPTCHA_DEFAULT_THEME_COLORS[self::$session->mode]['color'];
            $borderColor = imagecolorallocate($placeholder, $color[0], $color[1], $color[2]);
        }

        // Copy the icons onto the placeholder.
        $xOffset = $iconOffset;
        for ($i = 0; $i < $iconCount; $i++) {
            // Get the icon image from the array. Use position to get the icon ID.
            $icon = $iconImages[self::$session->icons[$i + 1]];
            $transformed = false;

            // Flip icon horizontally, if enabled.
            if (self::$options::flipImageHorizontally() && mt_rand(1, 2) === 1) {
                imageflip($icon, IMG_FLIP_HORIZONTAL);
                $transformed = true;
            }

            // Flip icon vertically, if enabled.
            if (self::$options::flipImageVertically() && mt_rand(1, 2) === 1) {
                imageflip($icon, IMG_FLIP_VERTICAL);
                $transformed = true;
            }

            // Rotate icon, if enabled.
            if (self::$options::rotateImage() && !$transformed) {
                $degree = mt_rand(1, 4);
                if ($degree !== 4 && $degree !== 2) { // Only if the 'degree' is not the same as what it would already be at.
                    $icon = imagerotate($icon, $degree * 90, 0);
                }
            }

            // Copy the icon onto the placeholder.
            imagecopy($placeholder, $icon, ($iconSize * $i) + $xOffset, 10, 0, 0, 30, 30);
            $xOffset += $iconOffsetAdd;

            // Add the vertical separator lines to the placeholder, if enabled.
            if ($borderEnabled && $i > 0) {
                imageline($placeholder, $iconLineSize * $i, 0, $iconLineSize * $i, 50, $borderColor);
            }
        }

        return $placeholder;
    }

    /**
     * Tries to load/initialize a {@see CaptchaSession} with the given captcha identifier.
     * When an existing session is found, it's data will be loaded, else a new session will be created.
     *
     * @param ?int $identifier The identifier of the captcha.
     */
    private static function createSession(Request $request, ?int $identifier = 0): void
    {
        // Load the captcha session for the current identifier.
        self::$session = new CaptchaSession($request, CaptchaSession::SESSION_NAME, $identifier);
    }

    /**
     * Returns the clicked icon position based on the X and Y position and the captcha width.
     *
     * @param $clickedXPos int The X position of the click.
     * @param $clickedYPos int The Y position of the click.
     * @param $captchaWidth int The width of the captcha.
     *
     * @return int The selected icon position.
     */
    private static function determineClickedIcon(int $clickedXPos, int $clickedYPos, int $captchaWidth, int $iconAmount): int
    {
        // Check if the clicked position is valid.
        if ($clickedXPos < 0 || $clickedXPos > $captchaWidth || $clickedYPos < 0 || $clickedYPos > 50) {
            return -1;
        }
        return (int)ceil($clickedXPos / ($captchaWidth / $iconAmount));
    }

    /**
     * Calculates the amount of times 1 or more other icons can be present in the captcha image besides the correct icon.
     * Each other icons should be at least present 1 time more than the correct icon. When calculating the icon
     * amount(s), the remainder of the calculation ($iconCount - $smallestIconCount) will be used.
     *
     * Example 1: When $smallestIconCount is 1, and the $iconCount is 8, the return value can be [3, 4].
     * Example 2: When $smallestIconCount is 2, and the $iconCount is 6, the return value can be [4]. This is because
     * dividing the remainder (4 / 2 = 2) is equal to the $smallestIconCount, which is not possible.
     * Example 3: When the $smallestIconCount is 2, and the $iconCount is 8, the return value will be [3, 3].
     *
     * @param int $iconCount The total amount of icons which will be present in the generated image.
     * @param ?int $smallestIconCount The amount of times the correct icon will be present in the generated image.
     * @return int[] The number of times an icon should be rendered onto the captcha image. Each value in the returned
     * array represents a new unique icon.
     */
    private static function calculateIconAmounts(int $iconCount, ?int $smallestIconCount = 1): array
    {
        $remainder = $iconCount - $smallestIconCount;
        $remainderDivided = $remainder / 2;
        $pickDivided = mt_rand(1, 2) === 1; // 50/50 chance.

        // If division leads to decimal.
        if (fmod($remainderDivided, 1) !== 0.0 && $pickDivided) {
            $left = floor($remainderDivided);
            $right = ceil($remainderDivided);

            // Only return the divided numbers if both are larger than the smallest number.
            if ($left > $smallestIconCount && $right > $smallestIconCount) {
                return [$left, $right];
            }
        } elseif ($pickDivided === true && $remainderDivided > $smallestIconCount) {
            // If no decimals: only return the divided numbers if it is larger than the smallest number.
            return [$remainderDivided, $remainderDivided];
        }

        // Return the whole remainder.
        return [$remainder];
    }
}
