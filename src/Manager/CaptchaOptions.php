<?php

namespace TBaronnat\IconRecaptchaBundle\Manager;

class CaptchaOptions
{
    public const THEME_LIGHT = 'light';
    public const THEME_DARK = 'dark';
    public const THEME_LEGACY_DARK = 'legacy-dark';
    public const THEME_LEGACY_LIGHT = 'legacy-light';

    public const CAPTCHA_FIELD_SELECTION = 'ic-hf-se';
    public const CAPTCHA_FIELD_ID = 'ic-hf-id';
    public const CAPTCHA_FIELD_HONEYPOT = 'ic-hf-hp';
    public const CAPTCHA_FIELD_TOKEN = '_iconcaptcha-token';

    public const CAPTCHA_DEFAULT_TIMEOUT = 60;
    public const CAPTCHA_DEFAULT_MAX_ATTEMPTS = 3;
    public const CAPTCHA_MIN_ICON_COUNT = 5;
    public const CAPTCHA_MAX_ICON_COUNT = 8;

    public const CAPTCHA_IMAGE_PLACEHOLDER_NAME = 'placeholder.png';
    public const CAPTCHA_DEFAULT_IMAGE_SIZE = 320;
    public const CAPTCHA_ICONS_FOLDER_COUNT = 180;
    public const CAPTCHA_ICON_SIZES = [5 => 50, 6 => 40, 7 => 30, 8 => 20];
    public const CAPTCHA_MAX_LOWEST_ICON_COUNT = [5 => 1, 6 => 2, 7 => 3, 8 => 3];
    public const CAPTCHA_DEFAULT_BORDER_COLOR = [240, 240, 240];
    
    public const CAPTCHA_DEFAULT_THEME_COLORS = [
        self::THEME_DARK => [
            'icons' => 'dark',
            'color' => self::CAPTCHA_DEFAULT_BORDER_COLOR
        ],
        self::THEME_LEGACY_DARK => [
            'icons' => 'light',
            'color' => self::CAPTCHA_DEFAULT_BORDER_COLOR
        ],
        self::THEME_LIGHT => [
            'icons' => 'light',
            'color' => [64, 64, 64]
        ],
        self::THEME_LEGACY_LIGHT => [
            'icons' => 'dark',
            'color' => [64, 64, 64]
        ],
    ];

    private static object $options;

    public function __construct(array $options)
    {
        self::$options = json_decode(json_encode($options));
    }

    public static function getSecret(): string
    {
        return self::$options->secret;
    }

    public static function getIconPath(): string
    {
        return self::$options->iconPath;
    }

    public static function getTranslationDomain(): string
    {
        return self::$options->transdomain;
    }

    public static function getMessages(): object
    {
        return self::$options->messages;
    }

    public static function getImageSize(): int
    {
        return (int)self::$options->image->size;
    }

    public static function getImageAmountMin(): int
    {
        return self::$options->image->amount->min;
    }

    public static function getImageAmountMax(): int
    {
        return self::$options->image->amount->max;
    }

    public static function flipImageHorizontally(): bool
    {
        return self::$options->image->flip->horizontally;
    }

    public static function flipImageVertically(): bool
    {
        return self::$options->image->flip->vertically;
    }

    public static function rotateImage(): bool
    {
        return self::$options->image->rotate;
    }

    public static function useImageBorder(): bool
    {
        return self::$options->image->border;
    }

    public static function getAttemptsTimeout(): int
    {
        return self::$options->attempts->timeout;
    }

    public static function getAttemptsAmount(): int
    {
        return self::$options->attempts->amount;
    }
}
