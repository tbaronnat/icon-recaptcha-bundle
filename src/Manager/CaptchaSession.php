<?php

namespace TBaronnat\IconRecaptchaBundle\Manager;

use Symfony\Component\HttpFoundation\Request;

class CaptchaSession
{
    public const SESSION_NAME = 'iconcaptcha';

    private array $session = [];

    /**
     * Creates a new CaptchaSession object. Session data regarding the
     * captcha (given identifier) will be stored and can be retrieved when necessary.
     *
     * @param string $key The name of the session key.
     * @param int $id The captcha identifier.
     */
    public function __construct(protected Request $request, protected string $key, protected int $id = 0)
    {
        // Try to load the captcha data from the session, if any data exists.
        $this->load($request);
    }

    /**
     * Loads the captcha's session data based on the earlier set captcha identifier.
     * @param Request $request
     */
    public function load(Request $request): void
    {
        if (self::exists($request, $this->key, $this->id)) {
            $this->session = $request->getSession()->get($this->key.$this->id);
        } else {
            $this->session = [
                'icons' => [], // The positions of the icon on the generated image.
                'iconIds' => [], // List of used icon IDs.
                'correctId' => 0, // The icon ID of the correct answer/icon.
                'mode' => CaptchaOptions::THEME_LIGHT, // The name of the theme used by the captcha instance.
                'requested' => false, // If the captcha image has been requested yet.
                'completed' => false, // If the captcha was completed (correct icon selected) or not.
                'attempts' => 0, // The number of times an incorrect answer was given.
            ];
        }
    }

    /**
     * This will clear the set hashes, and reset the icon
     * request counter and last clicked icon.
     */
    public function clear(): void
    {
        $this->session['icons'] = [];
        $this->session['iconIds'] = [];
        $this->session['correctId'] = 0;
        $this->session['requested'] = false;
        $this->session['completed'] = false;
        $this->session['attempts'] = 0;
    }

    /**
     * Destroys the captcha session.
     */
    public function destroy(Request $request): void
    {
        $request->getSession()->remove($this->key.$this->id);
    }

    /**
     * Saves the current data to the session. The data will be stored in an array.
     */
    public function save(Request $request): void
    {
        // Write the data to the session.
        $request->getSession()->set($this->key.$this->id, $this->session);
    }

    /**
     * Checks if the given captcha identifier has session data stored.
     *
     * @param string $key The name of the session key.
     * @param int $id The captcha identifier.
     *
     * @return boolean TRUE if any session data exists, FALSE if not.
     */
    public static function exists(Request $request, string $key, int $id): bool
    {
        return $request->getSession()->has($key.$id);
    }

    /**
     * Retrieves data from the session based on the given property name.
     * @param string $key The name of the property in the session which should be retrieved.
     * @return mixed The data in the session, or NULL if the key does not exist.
     */
    public function __get(string $key): mixed
    {
        return $this->session[$key] ?? null;
    }

    /**
     * Set a value of the captcha session.
     * @param string $key The name of the property in the session which should be set.
     * @param mixed $value The value which should be stored.
     */
    public function __set(string $key, mixed $value): void
    {
        $this->session[$key] = $value;
    }
}
