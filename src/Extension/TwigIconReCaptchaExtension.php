<?php

namespace TBaronnat\IconRecaptchaBundle\Extension;

use TBaronnat\IconRecaptchaBundle\Manager\IconReCaptcha;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigIconReCaptchaExtension extends AbstractExtension
{
    public function __construct(private readonly IconReCaptcha $iconReCaptcha)
    {}

    public function getFunctions(): array
    {
        return [
            new TwigFunction('iconReCaptchaToken', [$this->iconReCaptcha::getCaptchaToken(), 'buildToken']),
        ];
    }
}
