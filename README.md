### Introduction
##### This bundle is a fork of this legacy repository https://github.com/fabianwennink/IconCaptcha-Plugin-jQuery-PHP, transformed to Symfony Bundle.


### How To use it
#### 1. Installation
##### a. Install ext-gd php extension
 ```
 sudo apt install php8.1-gd
 
 Think to enable gd extension on your php.ini file
 ```

##### b. Install vendor
 ```
 composer require tbaronnat/icon-recaptcha-bundle
 ```
In composer.json, add bundle to autoload section:

```
    "autoload": {
        "psr-4": {
            "App\\": "src/",
            "TBaronnat\\IconRecaptchaBundle\\": "vendor/tbaronnat/icon-recaptcha-bundle/src/"
        }
    },
```
In your route.yaml file, import routing resource:

```
recaptcha:
  resource: '@TBaronnatIconRecaptchaBundle/Resources/config/routing.yaml'

```

#### 2. Configure options on your own tbaronnat_icon_recaptcha.yaml file
To understand all options uses for php or javascript, read this documentation :
https://github.com/fabianwennink/IconCaptcha-Plugin-jQuery-PHP/wiki/How-To-Use#options

#### 3. Needs to override file vendor/tbaronnat/icon-recaptcha-bundle/src/Resources/views/theme/js_and_css.html.twig to include javascript and css files.

###### For example, if you are using webpack-encore-bundle, you could do something like this:

```
webpack.config.js:
    Encore
        .copyFiles({
            from: './vendor/tbaronnat/icon-recaptcha-bundle/assets/css/',
            to: 'vendor/icon-recaptcha/[name].[ext]'
        })
        .copyFiles({
            from: './vendor/tbaronnat/icon-recaptcha-bundle/assets/js/',
            to: 'vendor/icon-recaptcha/[name].[ext]'
        })
```


###### Then, create directory in:
```
%kernel.project_dir%/templates/bundles/TBaronnatIconRecaptchaBundle/theme/
```

###### Create file js_and_css.html.twig with this content :
```
    (Ideally, defer css and js)
    <script src="{{ asset('/build/vendor/icon-recaptcha/icon-captcha.min.js') }}" async defer></script>
    <link rel="preload"
          href="{{ asset('/build/vendor/icon-recaptcha/icon-captcha.min.css')}}"
          as="style"
          onload="this.onload=null;this.rel='stylesheet';"
    >
```

###### You can also change default configuration of recaptcha rendering with overwriting ``config.html.twig`` or ``init.html.twig`` 

#### 4. On your form type, add IconRecaptchaType:
```
$builder->add('recaptcha', IconReCaptchaType::class);
```

#### 5. Reset recaptcha when error:
```
// If you want to reset the recaptcha du to any error during form submit, do this:

IconCaptcha.reset();
```
